﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="23008000">
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Day 01" Type="Folder">
			<Item Name="Day 01 - Part 1.vi" Type="VI" URL="../Day 01/Day 01 - Part 1.vi"/>
			<Item Name="Day 01 - Part 2.vi" Type="VI" URL="../Day 01/Day 01 - Part 2.vi"/>
		</Item>
		<Item Name="Day 02" Type="Folder">
			<Item Name="Day 02.vi" Type="VI" URL="../Day 02/Day 02.vi"/>
		</Item>
		<Item Name="Day 03" Type="Folder">
			<Item Name="Day 03.vi" Type="VI" URL="../Day 03/Day 03.vi"/>
		</Item>
		<Item Name="Day 04" Type="Folder">
			<Item Name="Day 04.vi" Type="VI" URL="../Day 04/Day 04.vi"/>
		</Item>
		<Item Name="Day 05" Type="Folder"/>
		<Item Name="Day 06" Type="Folder"/>
		<Item Name="Day 07" Type="Folder"/>
		<Item Name="Day 08" Type="Folder"/>
		<Item Name="Day 09" Type="Folder"/>
		<Item Name="Day 10" Type="Folder"/>
		<Item Name="Day 11" Type="Folder"/>
		<Item Name="Day 12" Type="Folder"/>
		<Item Name="Day 13" Type="Folder"/>
		<Item Name="Day 14" Type="Folder"/>
		<Item Name="Day 15" Type="Folder"/>
		<Item Name="Day 16" Type="Folder"/>
		<Item Name="Day 17" Type="Folder"/>
		<Item Name="Day 18" Type="Folder"/>
		<Item Name="Day 19" Type="Folder"/>
		<Item Name="Day 20" Type="Folder"/>
		<Item Name="Day 21" Type="Folder"/>
		<Item Name="Day 22" Type="Folder"/>
		<Item Name="Day 23" Type="Folder"/>
		<Item Name="Day 24" Type="Folder"/>
		<Item Name="Day 25" Type="Folder"/>
		<Item Name="General SubVIs" Type="Folder">
			<Item Name="Create Permutations.vi" Type="VI" URL="../SubVIs/Create Permutations.vi"/>
			<Item Name="String to 2D Integer Array.vim" Type="VI" URL="../SubVIs/String to 2D Integer Array.vim"/>
			<Item Name="Surrounding Coordinates.vi" Type="VI" URL="../SubVIs/Surrounding Coordinates.vi"/>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
